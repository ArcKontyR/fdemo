package com.example.fdemo

import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Switch
import android.widget.Toast
import com.example.fdemo.net.ApiRet
import com.example.fdemo.net.MyRetrofit
import retrofit2.Call
import retrofit2.Response
import java.util.regex.Pattern.compile

class activity_signin : AppCompatActivity() {
    lateinit var email: EditText
    lateinit var password: EditText
    lateinit var switch: Switch
    lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        email = findViewById(R.id.email_signup)
        password = findViewById(R.id.password_signup)
        sharedPreferences = getSharedPreferences("main", MODE_PRIVATE)
        switch = findViewById(R.id.save_profile_switch)
    }

    val pattern = ("[a-zA-Z0-9]{0,25}" +
            "\\@" +
            "[a-z]{0,10}" +
            "\\." +
            "[a-z]{0,10}")

    fun EmailValid (email:String):Boolean{
        return compile (pattern).matcher(email).matches()}

    fun login(view:View){
        if (email.text.toString().isNotEmpty() && password.text.toString().isNotEmpty()){
            if (EmailValid(email.text.toString())){
                val log = MyRetrofit().getRetrofit()
                val getApi = log.create(ApiRet::class.java)
                var hashMap:HashMap<String,String> = HashMap<String,String>()
                hashMap.put("email",email.text.toString())
                hashMap.put("password",password.text.toString())
                val log_call: retrofit2.Call<login> = getApi.getAuth(hashMap)
                log_call.enqueue(object:retrofit2.Callback<login>{
                    override fun onResponse(call: Call<login>, response: Response<login>) {
                        if (response.isSuccessful){
                            val editor = sharedPreferences.edit()
                            editor.putString("email", response.body()?.email)
                            editor.putString("avatar",response.body()?.avatar)
                            editor.putString("name", response.body()?.nickName)
                            editor.putBoolean("save", switch.isChecked)
                            editor.apply()
                            val intent = Intent(this@activity_signin,activity_menu::class.java)
                            startActivity(intent)
                        } else {
                            val alert = AlertDialog.Builder(this@activity_signin)
                                .setTitle("Ошибка входа")
                                .setMessage("Неверный пароль")
                                .setPositiveButton("OK",null)
                                .create()
                                .show()
                        }
                    }

                    override fun onFailure(call: Call<login>, t: Throwable) {
                        Toast.makeText(this@activity_signin, t.message, Toast.LENGTH_LONG).show()
                        val editor = sharedPreferences.edit()
                        editor.putString("email", email.text.toString())
                        editor.apply()
                        val intent = Intent(this@activity_signin,activity_menu::class.java)
                        startActivity(intent)
                    }
                })

            } else
            {
                val alert = AlertDialog.Builder(this)
                    .setTitle("Ошибка входа")
                    .setMessage("Email некорректен")
                    .setPositiveButton("OK",null)
                    .create()
                    .show()
            }
        } else
        {
            val alert = AlertDialog.Builder(this)
                .setTitle("Ошибка входа")
                .setMessage("Поля должны быть заполнены")
                .setPositiveButton("OK",null)
                .create()
                .show()
        }
    }
    fun signup(view:View){
        val intent = Intent(this,activity_signup::class.java)
        startActivity(intent)
    }

    fun check(view: View){
        if (switch.isChecked){
            val editor = sharedPreferences.edit()
            editor.putBoolean("save",true)
            editor.apply()
        } else{
            val editor = sharedPreferences.edit()
            editor.putBoolean("save",false)
            editor.apply()
        }
    }
}