package com.example.fdemo

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import java.util.regex.Pattern.compile

class activity_signup : AppCompatActivity() {

    lateinit var name: EditText
    lateinit var email: EditText
    lateinit var password: EditText
    lateinit var password_rep:EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        name = findViewById(R.id.name_signup)
        email = findViewById(R.id.email_signup)
        password = findViewById(R.id.password_signup)
        password_rep = findViewById(R.id.password_rep)
    }
    val pattern = ("[a-zA-Z0-9]{0,25}" +
            "\\@" +
            "[a-z]{0,10}" +
            "\\." +
            "[a-z]{0,10}")

    fun EmailValid (email:String):Boolean{
        return compile(pattern).matcher(email).matches()}
    fun signup(view:View){
        if (name.text.toString().isNotEmpty() &&
            email.text.toString().isNotEmpty() &&
            password.text.toString().isNotEmpty() &&
            password_rep.text.toString().isNotEmpty())
            {
            if (password.text.toString().equals(password_rep.text.toString()))
                {
                if (EmailValid(email.text.toString()))
                    {
                        val intent = Intent(this,activity_menu::class.java)
                        startActivity(intent)
                    } else
                    {
                        val alert = AlertDialog.Builder(this)
                            .setTitle("Ошибка входа")
                            .setMessage("Email некорректен")
                            .setPositiveButton("OK",null)
                            .create()
                            .show()
                    }
                } else
                {
                    val alert = AlertDialog.Builder(this)
                        .setTitle("Ошибка входа")
                        .setMessage("Пароли не совпадают")
                        .setPositiveButton("OK",null)
                        .create()
                        .show()
                }
            } else
            {
                val alert = AlertDialog.Builder(this)
                    .setTitle("Ошибка входа")
                    .setMessage("Поля должны быть заполнены")
                    .setPositiveButton("OK",null)
                    .create()
                    .show()
            }
    }
}