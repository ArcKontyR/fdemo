package com.example.fdemo

data class guide(val title:String, val description:String, val image:Int)

data class login(val id:String,
                 val email:String,
                 val nickName:String,
                 val avatar: String,
                 val token: String)

data class feelings(val success:Boolean, val data: ArrayList<data_feelings>)
data class data_feelings(val id:Int, val title:String, val image:String, val position:Int)

object list {
    val list = arrayListOf(
        guide(
            "Быстрая доставка!",
            "Burger free - приложение в котором вы можете заказывать бургеры онлайн и забрать его в ближайшей бургерной",
            R.drawable.guideicon1
        ),
        guide(
            "Быстрая доставка!",
            "Burger free - приложение в котором вы можете заказывать бургеры онлайн и забрать его в ближайшей бургерной",
            R.drawable.guideicon2
        )
    )
}
