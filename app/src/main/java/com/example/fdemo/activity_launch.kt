package com.example.fdemo

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer

class activity_launch : AppCompatActivity() {
    lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
        sharedPreferences = getSharedPreferences("main", MODE_PRIVATE)
        val timer = object : CountDownTimer(1000,100){
            override fun onTick(p0: Long) {

            }

            override fun onFinish() {
            if (sharedPreferences.getBoolean("save",false)){
                val intent = Intent(this@activity_launch, activity_menu::class.java )
                startActivity(intent)
            } else{
                val intent = Intent(this@activity_launch, activity_guide::class.java )
                startActivity(intent)
            }
            }

        }
        timer.start()
        finish()
    }
}