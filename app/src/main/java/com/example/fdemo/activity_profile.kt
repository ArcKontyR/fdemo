package com.example.fdemo

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

class activity_profile : AppCompatActivity() {

    lateinit var email: TextView
    lateinit var name: TextView
    lateinit var avatar: ImageView

    lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        sharedPreferences = getSharedPreferences("main", MODE_PRIVATE)
        email = findViewById(R.id.activity_profile_email)
        name = findViewById(R.id.activity_profile_name)
        avatar = findViewById(R.id.activity_profile_avatar)
        email.text = sharedPreferences.getString("email",null)!!
        name.text = sharedPreferences.getString("name", "Unknown")
        Glide.with(this).load(sharedPreferences.getString("avatar",R.drawable.default_profile_avatar.toString())).circleCrop().into(avatar)
    }

    fun exit(view:View){
        val editor = sharedPreferences.edit()
        editor.putString("email",null)
        editor.putString("name",null)
        editor.putString("avatar",null)
        editor.putBoolean("save",false)
        editor.apply()
        val intent = Intent(this,activity_signin::class.java)
        startActivity(intent)
        finish()
    }

    fun back(view: View){
        finish()
    }
}