package com.example.fdemo

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.fdemo.databinding.ActivityMenuBinding

class activity_menu : AppCompatActivity() {

    private lateinit var binding: ActivityMenuBinding
    lateinit var email: TextView
    lateinit var name: TextView
    lateinit var avatar: ImageView
    lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPreferences = getSharedPreferences("main", MODE_PRIVATE)
        email = findViewById(R.id.profile_email)
        name = findViewById(R.id.profile_name)
        avatar = findViewById(R.id.profile_avatar)
        val email_ =  sharedPreferences.getString("email",null)
        val avatar_ = sharedPreferences.getString("avatar",null)
        val name_ = sharedPreferences.getString("name","Unknown")

        email.text = email_
        name.text = name_
        if (!avatar_.equals(null)) {
            Glide.with(applicationContext).load(avatar_).circleCrop().into(avatar)
        }

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_menu)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        navView.setupWithNavController(navController)
    }

    fun profile(view: View){
        val intent = Intent(this,activity_profile::class.java)
        startActivity(intent)
    }
}