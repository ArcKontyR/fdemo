package com.example.fdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Adapter
import android.widget.Button
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class activity_guide : AppCompatActivity() {
    lateinit var viewpager: ViewPager2
    lateinit var tab: TabLayout
    lateinit var button: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide)
        viewpager = findViewById(R.id.viewpager)
        tab = findViewById(R.id.tablayout)
        button = findViewById(R.id.button)

        viewpager.adapter = viewpager_adapter(this, list.list)
        TabLayoutMediator(tab, viewpager) { _, _ -> }.attach()

        viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){

            override fun onPageSelected(position: Int) {
                when(position){
                    1->{
                        button.text = "Начать"
                    } else->
                {
                    button.text = "Далее"
                }
                }
            }
        })
    }

    fun guide(view: View)
    {
        when(viewpager.currentItem)
        {
            1->startActivity(Intent(this,activity_signin::class.java))
            else-> viewpager.currentItem++
        }
    }
}