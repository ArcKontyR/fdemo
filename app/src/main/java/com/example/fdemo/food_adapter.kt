package com.example.fdemo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class food_adapter(val context: Context, val list:ArrayList<data_feelings>):RecyclerView.Adapter<food_adapter.MyVH>() {
    class MyVH(itemView:View):RecyclerView.ViewHolder(itemView) {
        val imageView:ImageView = itemView.findViewById(R.id.image_food)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVH {
        val root = LayoutInflater.from(context).inflate(R.layout.food_adapter,parent,false)
        return MyVH(root)
    }

    override fun onBindViewHolder(holder: MyVH, position: Int) {
        Glide.with(context).load(list[position].image).into(holder.imageView)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}