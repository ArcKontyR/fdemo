package com.example.fdemo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class viewpager_adapter(val context: Context, val list:ArrayList<guide>): RecyclerView.Adapter<viewpager_adapter.MyVH>() {
    class MyVH(itemView: View): RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.guide_title)
        val image: ImageView = itemView.findViewById(R.id.guide_image)
        val desc: TextView = itemView.findViewById(R.id.guide_desc)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVH {
        val root = LayoutInflater.from(context).inflate(R.layout.viewpager_adapter, parent,false)
        return MyVH(root)
    }

    override fun onBindViewHolder(holder: MyVH, position: Int) {
        holder.title.text = list[position].title
        holder.desc.text = list[position].description
        holder.image.setImageResource(list[position].image)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}