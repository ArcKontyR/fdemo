package com.example.fdemo.ui.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fdemo.R
import com.example.fdemo.databinding.FragmentMenuBinding
import com.example.fdemo.feelings
import com.example.fdemo.food_adapter
import com.example.fdemo.net.ApiRet
import com.example.fdemo.net.MyRetrofit
import retrofit2.Call
import retrofit2.Response

class MenuFragment : Fragment() {

    private var _binding: FragmentMenuBinding? = null

    lateinit var foodRecyclerView: RecyclerView
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMenuBinding.inflate(inflater, container, false)

        foodRecyclerView = binding.root.findViewById(R.id.food_rec)

        val feel = MyRetrofit().getRetrofit()
        val api_ret = feel.create(ApiRet::class.java)
        val feel_call:retrofit2.Call<feelings> = api_ret.getFeel()
        feel_call.enqueue(object : retrofit2.Callback<feelings>{
            override fun onResponse(call: Call<feelings>, response: Response<feelings>) {
                if (response.isSuccessful){
                    val data = response.body()?.data
                    foodRecyclerView.layoutManager = GridLayoutManager(requireContext(),2)
                    foodRecyclerView.adapter = data?.let { food_adapter(requireContext(), it) }
                }
            }

            override fun onFailure(call: Call<feelings>, t: Throwable) {
                Toast.makeText(requireContext(), t.localizedMessage, Toast.LENGTH_SHORT).show()
            }

        })

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}